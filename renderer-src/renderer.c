#include "common.h"
#include "cJSON/cJSON.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>


#define LANGCODELEN 2

#define TEMPLATE_VAL_OPEN "%%"
#define TEMPLATE_VAL_CLOSE "%%"

static int render_translatable(char * errmsg, FILE * out, const cJSON * json, const char * lang) {
	int res = 0;
	cJSON * tmp = NULL;
	if (out == NULL || json == NULL || lang == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}
	
	if (cJSON_IsString(json)) {
		if (json->valuestring == NULL) {
			sprintf(errmsg, "%s: string is NULL", __func__);
			goto end;
		}
		if (fputs(json->valuestring, out) == EOF) {
			sprintf(errmsg, "%s: failed writing to ouput file", __func__);
			goto end;
		}
	} else if (cJSON_IsObject(json)) {
		if ((tmp = cJSON_GetObjectItemCaseSensitive(json, lang)) == NULL) {
			char * tmpprint;
			tmpprint = cJSON_Print(json);
			fprintf(stderr, "%s: [DEBUG] json: \"%s\"\n", __func__, tmpprint);
			free(tmpprint);
			sprintf(errmsg, "%s: json doesn't contain language: %s", __func__, lang);
			goto end;
		}
		if (tmp->valuestring == NULL) {
			sprintf(errmsg, "%s: string is NULL", __func__);
			goto end;
		}
		if (fputs(tmp->valuestring, out) == EOF) {
			sprintf(errmsg, "%s: failed writing to ouput file", __func__);
			goto end;
		}
	} else {
		sprintf(errmsg, "%s: json is of unknown type", __func__);
		goto end;
	}


	res = 1;
end:
	return res;
}

static int render_template(char * errmsg, FILE * out, const char * template, const char * template_for_arrays, const cJSON * json, const char * lang) {
	int res = 0;
	const char * cur = template;
	char * val_open = NULL;
	char * val_close = NULL;
	char * val_name = NULL;
	size_t val_len = 0;
	cJSON * tmp = NULL;
	cJSON * tmp_translatable = NULL;
	cJSON * arrayitem = NULL;

	//if (out == NULL || template == NULL || template_for_arrays == NULL || json == NULL || lang == NULL) {
	if (out == NULL || template == NULL || json == NULL || lang == NULL) {
		sprintf(errmsg, "%s: required argument not set", __func__);
		goto end;
	}

	if (!cJSON_IsObject(json)) {
		sprintf(errmsg, "%s: json isn't an object", __func__);
		goto end;
	}

	while ((val_open = strstr(cur, TEMPLATE_VAL_OPEN)) != NULL) {
		val_close = strstr(val_open + 1, TEMPLATE_VAL_CLOSE);
		if (val_close == NULL) {
			sprintf(errmsg, "%s: value tag opened but never closed", __func__);
			goto end;
		}
		val_len = val_close - val_open - strlen(TEMPLATE_VAL_CLOSE);
		//fprintf(stderr, "%s: [DEBUG] val_len: %ld val_open: \"%30.30s\"\n", __func__, val_len, val_open);
		fprintf(stderr, "%s: [DEBUG] val_len: %ld\n", __func__, val_len);
	
		if ((val_name = malloc(sizeof(char) * (val_len + 1))) == NULL) {
			sprintf(errmsg, "%s: failed allocating val_name! val_len: %ld", __func__, val_len);
			goto end;
		}

		strncpy(val_name, val_open + strlen(TEMPLATE_VAL_OPEN), val_len);
		val_name[val_len] = '\0';

		fprintf(stderr, "%s: [DEBUG] val_name: \"%s\"\n", __func__, val_name);

		// write all characters up to val_open to output file
		while (cur != val_open) {
			if (fputc(*cur, out) == EOF) {
				sprintf(errmsg, "%s: failed writing '%c' to output file", __func__, *cur);
				goto end;
			}
			cur++;
		}
		
		if (strchr(val_name, '{') == NULL) {
			// val_name points to a value
			if ((tmp = cJSON_GetObjectItemCaseSensitive(json, val_name)) == NULL) {
				fprintf(stderr, "%s: [DEBUG] val_name: \"%s\"\n", __func__, val_name);
				sprintf(errmsg, "%s: Couldn't find val_name in json", __func__);
				goto end;
			}

			if (cJSON_IsArray(tmp)) {
				// value is an array
				if (template_for_arrays == NULL) {
					sprintf(errmsg, "%s: array found but template_for_arrays not set", __func__);
					goto end;
				}
				cJSON_ArrayForEach(arrayitem,tmp) {
					if (!render_template(errmsg, out, template_for_arrays, NULL, arrayitem, lang)) {
						goto end;
					}
				
				}
			} else {
				// render value as translatable
				if (!render_translatable(errmsg,out,tmp,lang)) {
					goto end;
				}
			}
		} else {
			// val_name is a translatable
			if ((tmp_translatable = cJSON_Parse(val_name)) == NULL) {
				fprintf(stderr, "%s: [DEBUG] val_name: \"%s\"\n", __func__, val_name);
				sprintf(errmsg, "%s: Couldn't parse val_name as json", __func__);
				goto end;
			}
			if (!render_translatable(errmsg,out,tmp_translatable,lang)) {
				goto end;
			}
		
			cJSON_Delete(tmp_translatable);
			tmp_translatable = NULL; // prevent double freeing
		}
		
		free(val_name);
		val_name = NULL; // prevent double freeing

		cur = val_close + strlen(TEMPLATE_VAL_CLOSE); // move cur to after val_close
	}

	// no more values in template
	// write the rest of template to the output file
	while (*cur != '\0') {
		if (fputc(*cur, out) == EOF) {
			sprintf(errmsg, "%s: failed writing '%c' to output file", __func__, *cur);
			goto end;
		}
		cur++;
	}



	res = 1;
end:
	free(val_name);
	cJSON_Delete(tmp_translatable);
	return res;
}

static void printerror(const char * errmsg) {
	if (errno)
		fprintf(stderr, COLOR_RED"ERROR: "COLOR_PURPLE"%s"COLOR_RESET": " COLOR_CYAN"%s"COLOR_RESET"\n", errmsg, strerror(errno));
	else
		fprintf(stderr, COLOR_RED"ERROR: "COLOR_PURPLE"%s"COLOR_RESET"\n", errmsg);
}

static void printusage(const char * prog) {
	printf("Usage: %s <games json file> <site template file> <game template file> <output file> <lang>\n", prog);
	printf("  Note: <lang> must be a two-letter language code\n");
}

int main(int argc, char ** argv) {
	int res = 1; // result; 1: error 0: success
	char errmsg[ERRMSGMAXSIZE];
	cJSON * games = NULL;
	char * template_site = NULL;
	char * template_game = NULL;
	FILE * output_file = NULL;
	char lang[LANGCODELEN + 1];
	
	errmsg[0] = '\0';
	lang[0] = '\0';

	if (argc != 6) {
		sprintf(errmsg, "Not enough or too many arguments!");
		printerror(errmsg);
		printusage(argv[0]);
		goto end;
	}

	// print arguments
	printf(COLOR_YELLOW"==>> renderer arguments: "COLOR_RESET"\n");
	printf(COLOR_YELLOW"  game json file     : "COLOR_CYAN"%s"COLOR_RESET"\n", argv[1]);
	printf(COLOR_YELLOW"  site template file : "COLOR_CYAN"%s"COLOR_RESET"\n", argv[2]);
	printf(COLOR_YELLOW"  game template file : "COLOR_CYAN"%s"COLOR_RESET"\n", argv[3]);
	printf(COLOR_YELLOW"  output file        : "COLOR_CYAN"%s"COLOR_RESET"\n", argv[4]);
	printf(COLOR_YELLOW"  lang               : "COLOR_CYAN"%s"COLOR_RESET"\n", argv[5]);


	// parse arguments
	if ((games = filetojson(argv[1])) == NULL) {
		sprintf(errmsg, "Failed reading games json file");
		printerror(errmsg);
		goto end;
	}

	if ((template_site = filetostr(argv[2])) == NULL) {
		sprintf(errmsg, "Failed reading site template file");
		printerror(errmsg);
		goto end;
	}

	if ((template_game = filetostr(argv[3])) == NULL) {
		sprintf(errmsg, "Failed reading game template file");
		printerror(errmsg);
		goto end;
	}

	if ((output_file = fopen(argv[4], "w")) == NULL) {
		sprintf(errmsg, "Failed opening output file");
		printerror(errmsg);
		goto end;
	}

	if (strlen(argv[5]) != LANGCODELEN) {
		sprintf(errmsg, "<lang> must be a two-letter language code");
		printerror(errmsg);
		goto end;
	}
	lang[0] = tolower(argv[5][0]);
	lang[1] = tolower(argv[5][1]);
	lang[2] = '\0';

	fprintf(stderr, COLOR_YELLOW"=> [DEBUG] template_site: "COLOR_RESET"\""COLOR_CYAN"%s"COLOR_RESET"\"\n", template_site);
	fprintf(stderr, COLOR_YELLOW"=> [DEBUG] template_game: "COLOR_RESET"\""COLOR_CYAN"%s"COLOR_RESET"\"\n", template_game);

	// render
	printf(COLOR_YELLOW"==>> rendering..."COLOR_RESET"\n");
	if (!render_template(errmsg, output_file, template_site, template_game, games, lang)) {
		printerror(errmsg);
		goto end;
	}
	printf(COLOR_GREEN"==>> Success!"COLOR_RESET"\n");

	res = 0; // success
end:
	cJSON_Delete(games);
	free(template_site);
	free(template_game);
	if (output_file)
		fclose(output_file);
	return res;
}
