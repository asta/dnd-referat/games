#ifndef COMMON_H_
#define COMMON_H_
#include "cJSON/cJSON.h"

#include <stdio.h>

/**
 * @brief ANSI Escape Sequence for the color black
 */
#define COLOR_BLACK "\033[0;30m"
/**
 * @brief ANSI Escape Sequence for the color red
 */
#define COLOR_RED "\033[0;31m"
/**
 * @brief ANSI Escape Sequence for the color green
 */
#define COLOR_GREEN "\033[0;32m"
/**
 * @brief ANSI Escape Sequence for the color yellow
 */
#define COLOR_YELLOW "\033[0;33m"
/**
 * @brief ANSI Escape Sequence for the color blue
 */
#define COLOR_BLUE "\033[0;34m"
/**
 * @brief ANSI Escape Sequence for the color purple
 */
#define COLOR_PURPLE "\033[0;35m"
/**
 * @brief ANSI Escape Sequence for the color cyan
 */
#define COLOR_CYAN "\033[0;36m"
/**
 * @brief ANSI Escape Sequence for the color white
 */
#define COLOR_WHITE "\033[0;37m"
/**
 * @brief ANSI Escape Sequence for resetting the color
 */
#define COLOR_RESET "\033[0m"

/**
 * @brief size of errmsg buffer
 */
#define ERRMSGMAXSIZE 1024

/**
 * @brief the length of a UUID
 */
#define UUIDLEN 36

/**
 * @brief macro for marking a variable as unused
 */
#define UNUSED(x) (void)(x)

int uuid_v4_gen(char *buffer);
int is_valid_uuid_v4(const char *buffer);

int is_valid_hex(char c);
int hex_to_int(char c);
char int_to_hex(int x);

int is_percent_encoded(const char * str);
char * percent_decode(const char * str);
char * percent_encode(const char * str);

int isprefix(const char *pre, const char *str);

char * filetostr(const char * path);

cJSON * filetojson(const char * path);

char * simple_strdup(const char * s);

void remove_whitespace(char * s);

int writestreamtofile(FILE * stream, const char * outpath);

int get_string_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * stringkey, char ** res , int can_be_null);
int get_number_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * numberkey, long * res);
int get_bool_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * boolkey, int * res);

#endif
