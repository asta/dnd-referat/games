#include "common.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>

#include "cJSON/cJSON.h"

/**
 * @file
 *
 * @brief A bunch of useful code.
 *
 */



/**
 * @brief Tests whether a given string is a valid UUIDv4.
 *
 * A UUIDv4 looks like this: 034d9d19-0182-4fd8-aa18-21ffd18bf956
 *
 *
 * @param[in] buffer the string to be tested
 * 
 * @returns whether buffer is a valid UUIDv4.
 * @retval 0 not a valid UUIDv4
 * @retval 1 a valid UUIDv4
 */
int is_valid_uuid_v4(const char *buffer) {
	int i;
	int counter = 0;

	if (buffer == NULL || strlen(buffer) != UUIDLEN)
		return 0;

/* 034d9d19-0182-4fd8-aa18-21ffd18bf956	 */
	for (i = 0; i < 8; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return 0;
	}
	if (buffer[counter++] != '-')
			return 0;
	for (i = 0; i < 4; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return 0;
	}
	if (buffer[counter++] != '-')
			return 0;
	for (i = 0; i < 4; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return 0;
	}
	if (buffer[counter++] != '-')
			return 0;
	for (i = 0; i < 4; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return 0;
	}
	if (buffer[counter++] != '-')
			return 0;
	for (i = 0; i < 12; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return 0;
	}
	return 1;
}

/**
 * @brief Tests whether a character is a valid hexadecimal character.
 *
 * Valid hexadecimal characters are: 0-9 a-f A-F
 *
 * @param[in] c the character to be tested
 *
 * @returns whether c is a valid hex character.
 * @retval 0 not a valid UUIDv4
 * @retval 1 a valid UUIDv4
 */
int is_valid_hex(char c) {
	return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}


/**
 * @brief Converts a hexadecimal character to a number.
 *
 * Basically '0'-'9' are mapped to 0-9 and 'a'-'f' or 'A'-'F' are mapped to 10-15.
 *
 * A non-hexadecimal character is just treated as '0'.
 *
 * @param[in] c the character
 *
 * @returns c as a number
 *
 *
 */
int hex_to_int(char c) {
	if (c >= '0' && c <= '9') {
		return c - '0';
	}
	c = (char)tolower(c);
	if (c >= 'a' && c <= 'f') {
		return c - 'a' + 10;
	}
	return 0;
}

/**
 * @brief Converts a number to an uppercase hexadecimal character.
 *
 * Basically 0-9 are mapped to '0'-'9' and 10-15 are mapped to 'A'-'F'.
 *
 * A number larger than 15 or smaller than 0 is just treated as 0.
 *
 * @param[in] x the number
 *
 * @returns x as an uppercase hexadecimal character
 *
 */
char int_to_hex(int x) {
	if (x >= 0 && x <= 9)
		return '0' + (char)x;
	if (x >= 10 && x <= 15)
		return 'a' + (char)x - (char)10;
	return '0';
}

/**
 * @brief Tests whether a given string is correctly percent encoded.
 *
 * Conforming to [RFC 3986 §2.1](https://datatracker.ietf.org/doc/html/rfc3986#section-2.1).
 *
 * @param[in] str the string to be tested
 * 
 * @returns whether str is correctly percent encoded.
 */
int is_percent_encoded(const char * str) {
	size_t len;
	int mode = 0; /* 0: expecting unreserved char or percent // 1: expecting first hex  // 2: expecting second hex */
	size_t i;
	char c;

	if (str == NULL)
		return 0;

	len = strlen(str);

	for (i = 0; i < len; i++) {
		c = str[i];
		switch (mode) {
		case 0:
			if (c == '%') {
				mode = 1;
				break;
			} else if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '-' || c == '.' || c == '_' || c == '~') {
				break;
			} else {
				return 0;
			}
		case 1:
			if (is_valid_hex(c)) {
				mode = 2;
				break;
			} else {
				return 0;
			}
		case 2:
			if (is_valid_hex(c)) {
				mode = 0;
				break;
			} else {
				return 0;
			}
		default:
			fprintf(stderr, "WARN: is_percent_encoded: unknown mode!!\n");
			return 0;
		}
	}

	return (mode == 0);
}

/**
 * @brief Decodes a percent encoded string.
 *
 * This function expects str to be conforming to [RFC 3986 §2.1](https://datatracker.ietf.org/doc/html/rfc3986#section-2.1).
 * You can test if str is valid using is_percent_encoded().
 *
 * @note The sequence "%00" may cause undefined behaviour as it will just null-terminate the string at that point.
 *
 * @param[in] str the percent encoded string to be decoded
 *
 * @return the decoded string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 */
char * percent_decode(const char * str) {
	char * res;
	int r = 0;
	int w = 0;
	char c;
	char buf[3];

	if (str == NULL)
		return NULL;

	res = malloc(sizeof(char) * (strlen(str) +1));
	if (res == NULL)
		return NULL;

	buf[2] = '\0';
	while ((c = str[r]) != '\0') {
		if (c == '%') {
			buf[0] = str[r+1];	
			buf[1] = str[r+2];	
			res[w] = (char)(strtol(buf,NULL,16));
			r+=2;
		} else {
			res[w] = str[r];
		}
		w++;
		r++;
	}
	res[w] = '\0';
	return res;
}

/**
 * @brief Percent encodes a string.
 *
 * Conforming to [RFC 3986 §2.1](https://datatracker.ietf.org/doc/html/rfc3986#section-2.1).
 *
 * @note This function cannot handle '\0' characters as those are used for indicating the end of a string in C.
 *
 * @param[in] str the string to be encoded
 *
 * @return the encoded string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 */
char * percent_encode(const char * str) {
	size_t ressize;
	char * res;
	int r = 0;
	int w = 0;
	char c;

	if (str == NULL)
		return NULL;

	ressize = strlen(str) + 1;
	res = malloc(sizeof(char) * ressize);
	if (res == NULL)
		return NULL;

	while ((c = str[r]) != '\0') {
		if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '-' || c == '.' || c == '_' || c == '~') {
			res[w] = str[r];
		} else {
			ressize += 2;
			res = realloc(res, ressize);
			if (res == NULL)
				return NULL;
			res[w]   = '%';
			res[w+1] = int_to_hex((unsigned char)(((unsigned char)(((unsigned char)c) & 0xf0)) >> 4) );
			res[w+2] = int_to_hex((unsigned char)(((unsigned char)(((unsigned char)c) & 0x0f))));
			/* printf("percent_encode: c: '%c' (int): %d (hex): %X\n", c, c, c); */
			w+=2;
		}
		w++;
		r++;
	}
	res[w] = '\0';
	return res;

}

/**
 * @brief Tests whether str starts with pre.
 *
 * @param[in] pre the expected prefix string
 * @param[in] str the string to be tested
 *
 * @returns whether str starts with pre.
 */
int isprefix(const char *pre, const char *str) {
	return strncmp(pre, str, strlen(pre)) == 0;
}


/**
 * @brief Reads a file into a string.
 *
 * Reads the file at path and converts it into a null-terminated string.
 *
 * @note This function is not suitable for binary files that may contain '\0' characters.
 *
 * @returns The files contents as a string.
 * @retval NULL an error occurred and errno might be set.
 *
 * The returned string must be freed using free().
 */
char * filetostr(const char * path) {
	char * buffer = NULL;
	size_t length;
	FILE * f = fopen (path, "rb");

	if (f) {
		fseek (f, 0, SEEK_END);
		length = (size_t)ftell(f);
		fseek (f, 0, SEEK_SET);
		buffer = malloc (sizeof(char) * (length +1));
		if (buffer) {
			buffer[length] = '\0';
			if (fread(buffer, 1, length, f)==0) {
				//fprintf(stderr, "Warn: fread might have failed\n");
				if (!feof(f) || ferror(f)) {
					free(buffer);
					buffer = NULL;
				}
			}
		}
		fclose(f);
	}

	return buffer;
}

/**
 * @brief Reads a JSON file.
 *
 * Reads the file at path and parses it into a cJSON object.
 *
 * @returns The files contents as a cJSON object.
 * @retval NULL an error occurred and errno might be set.
 *
 * The returned cJSON object must be freed using cJSON_Delete().
 */
cJSON * filetojson(const char * path) {
	char * buffer = NULL;
	cJSON * json = NULL;
	if ((buffer = filetostr(path)) == NULL) {
		return NULL;
	}
	json = cJSON_Parse(buffer); // might be NULL on failure
	free(buffer);
	return json;
}





/**
 * @brief Duplicates a string.
 *
 * Exactly the same behaviour as the strdup() function defined in string.h.
 *
 * @param[in] s the string to be duplicated
 *
 * @returns the duplicated string
 * @retval NULL an error occurred and errno might be set
 */
char * simple_strdup(const char * s) {
	size_t len;
	char * res;
	if (s == NULL)
		return NULL;

	len = strlen(s);
	res = malloc(sizeof(char) * (len + 1));
	if (res == NULL)
		return NULL;
	res[0] = '\0';
	strcat(res, s);
	return res;
}




/**
 * @brief Removes all whitespace characters from a string.
 *
 * @param[in,out] s the string
 *
 */
void remove_whitespace(char * s) {
	const char* d = s;
	if (s == NULL || *s == '\0')
		return;
	do {
		while (*d != '\0' && isspace(*d)) {
			d++;
		}
		*s = *d;
		if (*s == '\0')
			return;
		s++;
		d++;
	} while (*s != '\0');
}



/**
 * @brief Writes a stream to the file located at outpath.
 *
 * The stream is read until EOF is reached.
 *
 * @param[in] stream the stream to be read from
 * @param[in] outpath path to the file were the output should be written to
 *
 * @return whether the function was successful
 * @retval 1 success
 * @retval 0 An error occurred and errno might be set
 *
 */
int writestreamtofile(FILE * stream, const char * outpath) {
	int res = 0;
	FILE * out = NULL;
	int c = '\0';

	if (stream == NULL || outpath == NULL) {
		goto end;
	}

	if ((out = fopen(outpath, "w")) == NULL) {
		goto end;
	}

	// this is slow, I know
	while (!feof(stream)) {
		c = fgetc(stream);
		if (fputc(c, out) == EOF) {
			goto end;
		}
	}

	res = 1;
end:
	fclose(out);
	return res;
}





/**
 * @brief Tries to retrieve the value of stringkey from json as a string.
 * 
 * Note: res will only contain the pointer to the string inside the json object and thus will be freed by calling cJSON_Delete on json.
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] json the json object that is supposed to contain stringkey 
 * @param[in] curjsonkey the name of json (used for error messages)
 * @param[in] stringkey the name of the key
 * @param[out] res pointer to the pointer where the pointer of the retrieved value should be stored
 * @param[in] can_be_null whether the value is allowed to be null
 *
 * @returns whether the function was successful.
 * @retval 1 success. a pointer to the value has been stored in res
 * @retval 0 failed retrieving the string and an error message has been written to errmsg
 */
int get_string_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * stringkey, char ** res , int can_be_null) {
	int success = 0;
	cJSON * tmp = NULL;
	
	if (errmsg == NULL || curjsonkey == NULL || json == NULL || stringkey == NULL || res == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	*res = NULL;

	if ((tmp = cJSON_GetObjectItemCaseSensitive(json, stringkey)) == NULL) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not defined", __func__, curjsonkey, stringkey);
		goto end;
	}
	if ((!cJSON_IsString(tmp) || tmp->valuestring == NULL) && (!can_be_null && !cJSON_IsNull(tmp)) ) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not a string%s", __func__, curjsonkey, stringkey, (can_be_null ? " and is not null" : ""));
		goto end;
	}
	if (can_be_null && cJSON_IsNull(tmp)) {
		*res = NULL;
	} else {
		*res = tmp->valuestring;
	}

	success = 1;
end:
	return success;
}


/**
 * @brief Tries to retrieve the value of numberkey from json as a long.
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] json the json object that is supposed to contain numberkey 
 * @param[in] curjsonkey the name of json (used for error messages)
 * @param[in] numberkey the name of the key
 * @param[out] res where the retrieved value should be stored
 *
 * @returns whether the function was successful.
 * @retval 1 success. a pointer to the value has been stored in res
 * @retval 0 failed retrieving the number and an error message has been written to errmsg
 */
int get_number_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * numberkey, long * res) {
	int success = 0;
	cJSON * tmp = NULL;
	
	if (errmsg == NULL || curjsonkey == NULL || json == NULL || numberkey == NULL || res == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	if ((tmp = cJSON_GetObjectItemCaseSensitive(json, numberkey)) == NULL) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not defined", __func__, curjsonkey, numberkey);
		goto end;
	}
	if (!cJSON_IsNumber(tmp)) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not a number", __func__, curjsonkey, numberkey);
		goto end;
	}
	*res = (long) tmp->valuedouble;

	success = 1;
end:
	return success;
}

/**
 * @brief Tries to retrieve the value of boolkey from json as an int.
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] json the json object that is supposed to contain boolkey 
 * @param[in] curjsonkey the name of json (used for error messages)
 * @param[in] boolkey the name of the key
 * @param[out] res where the retrieved value should be stored
 *
 * @returns whether the function was successful.
 * @retval 1 success. a pointer to the value has been stored in res
 * @retval 0 failed retrieving the bool and an error message has been written to errmsg
 */
int get_bool_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * boolkey, int * res) {
	int success = 0;
	cJSON * tmp = NULL;
	
	if (errmsg == NULL || curjsonkey == NULL || json == NULL || boolkey == NULL || res == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	if ((tmp = cJSON_GetObjectItemCaseSensitive(json, boolkey)) == NULL) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not defined", __func__, curjsonkey, boolkey);
		goto end;
	}
	if (!cJSON_IsBool(tmp)) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not a bool", __func__, curjsonkey, boolkey);
		goto end;
	}
	*res = cJSON_IsTrue(tmp);

	success = 1;
end:
	return success;
}

