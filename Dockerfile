# This file is a template, and might need editing before it works on your project.
FROM httpd:buster

COPY . /app
WORKDIR /app

RUN apt update && apt install -y git gcc make

RUN make build-renderer
RUN make build-site
RUN make deploy-site

RUN cp -r /app/public/* /usr/local/apache2/htdocs/
EXPOSE 80
