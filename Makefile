# Note: languages are represented by a two-letter code
LANGUAGES:= de en
DEFAULTLANGUAGE:= de

DEPLOYDIR:= public
BUILDDIR:= build
GAMEJSON:= games-data/games.json

define RENDERSITE
./renderer $(GAMEJSON) templates/site.html templates/game.html $(BUILDDIR)/$(2) $(1) ;
endef

.PHONY: default
default: all

.PHONY: all
all: build-renderer build-site deploy-site

.PHONY: clean
clean:
	make -C renderer-src/ clean
	$(RM) -r $(BUILDDIR)

.PHONY: build-renderer
build-renderer:
	@echo "=> Building renderer"
	make -C renderer-src/

.PHONY: games-data
games-data:
	git submodule update --remote games-data/

.PHONY: build-site
build-site: games-data
	@echo "=> Building site"
	mkdir -p $(BUILDDIR)/
	$(foreach lang,$(LANGUAGES),$(call RENDERSITE,$(lang),$(lang).html))
	$(call RENDERSITE,$(DEFAULTLANGUAGE),index.html)

.PHONY: deploy-site
deploy-site: games-data
	@echo "=> Deploying site"
	mkdir -p $(DEPLOYDIR)/
	cp -rvf static/ $(DEPLOYDIR)/
	cp -rvf games-data/img/ $(DEPLOYDIR)/
	cp -rvf $(GAMEJSON) $(DEPLOYDIR)/
	cp -rvf $(BUILDDIR)/* $(DEPLOYDIR)/
