# Spieleseite

http://games.asta.uni-goettingen.de/
https://asta.pages.gwdg.de/dnd-referat/games/

## Was ist wo und wozu?
- [games-data/](https://gitlab.gwdg.de/asta/dnd-referat/games-data/) beinhaltet Daten zu den Spielen.
	- [games-data/games.json](https://gitlab.gwdg.de/asta/dnd-referat/games-data/-/blob/master/games.json) beinhaltet die Listen aller Spiele. Ist auch auf der Webseite unter `games.json`.
	- [games-data/img/](https://gitlab.gwdg.de/asta/dnd-referat/games-data/-/tree/master/img) beinhaltet spielespezifische Bilder. Ist auch auf der Webseite unter `img/`.
- [templates/](templates/) beinhaltet die Templates mit denen die Seite gebaut wird.
	- `templates/site.html` beinhaltet das Template für die Seite.
	- `templates/game.html` beinhaltet das Template für die einzelnen Spiele.
- [static/](static/) beinhaltete statische Inhalte. Ist auch auf der Webseite unter `static/`.
- [dnd-referat/](dnd-referat/) ist nur dafür da, dass man die Seite auch lokal gehostet korrekt verwenden kann.
- [Makefile](Makefile) beschreibt wie die Seite gebaut werden soll.
- [build/](build/) wird erst beim Bauen der Seite generiert und beinhaltet den dynamisch generierten Teil der Seite.
- [public/](public/) wird erst beim Bauen der Seite generiert und beinhaltet die vollständige Seite.
- [renderer-src/](renderer-src/) beinhaltet den Sourcecode für den Renderer der Seite.
- [renderer](renderer) ist ein SymLink auf das `renderer`-Programm.

## How-To: Seite lokal hosten
Wenn man an der Seite lokal arbeiten will, ist es häufig praktisch die Seite auch lokal hosten zu können.

Hierfür benötigt man die folgende Software:
- [Git](https://git-scm.com/)
- [Make](https://en.wikipedia.org/wiki/Make_(software))
- [GCC](https://gcc.gnu.org/)
- [Python 3](https://www.python.org/)

Nun muss man die folgenden Schritte ausführen:
1. Das Projekt klonen (WICHTIG: Beachte das `--recurse-submodules`!): `git clone --recurse-submodules git@gitlab.gwdg.de:asta/dnd-referat/games.git`
1. In den Projektordner gehen: `cd games`
1. Die Seite einmal bauen: `make`
1. Die Seite hosten: `python3 -m http.server 8080 --bind 127.0.0.1` (Dieser Schritt sollte man womöglich in einem extra Terminal __aber noch im `games` Ordner__ ausführen.)
1. Im Browser [http://127.0.0.1:8080/dnd-referat/games/](http://127.0.0.1:8080/dnd-referat/games/) öffnen.

Jetzt kann man Änderungen an der Seite durchführen (`games.json`, `templates/` oder `static/` bearbeiten).
Um die Änderungen lokal wirksam zu machen, muss man die Seite wieder neubauen mit `make` und im Browser die Seite neuladen.
Zum Schluss muss man die Änderungen nur noch mit `git add` zur Stage hinzufügen, mit `git commit` commiten und mit `git push` pushen.

Nachdem man die Änderungen gepusht hat, sollte die Gitlab CI Pipeline durchlaufen und nach einigen Minuten sind die Änderungen dann auch live.

## How-To: neues Spiel hinzufügen
TODO

## How-To: neue Sprache hinzufügen
TODO

## How-To: Template bearbeiten
TODO
